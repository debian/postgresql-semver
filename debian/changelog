postgresql-semver (0.40.0-2) unstable; urgency=medium

  * Upload for PostgreSQL 17.
  * Restrict to 64-bit architectures.
  * Mark postgresql-all as <!nocheck>.

 -- Christoph Berg <myon@debian.org>  Sun, 15 Sep 2024 13:05:01 +0200

postgresql-semver (0.40.0-1) unstable; urgency=medium

  * New upstream version 0.40.0.

 -- Christoph Berg <myon@debian.org>  Thu, 08 Aug 2024 18:20:41 +0200

postgresql-semver (0.32.1-2) unstable; urgency=medium

  * Upload for PostgreSQL 16.
  * Use ${postgresql:Depends}.

 -- Christoph Berg <myon@debian.org>  Mon, 18 Sep 2023 22:20:06 +0200

postgresql-semver (0.32.1-1) unstable; urgency=medium

  * New upstream version 0.32.1.

 -- Christoph Berg <myon@debian.org>  Mon, 07 Aug 2023 20:14:14 +0200

postgresql-semver (0.32.0-2) unstable; urgency=medium

  * Include varatt.h on PG16.

 -- Christoph Berg <myon@debian.org>  Mon, 31 Jul 2023 12:01:49 +0200

postgresql-semver (0.32.0-1) unstable; urgency=medium

  * New upstream version 0.32.0.
  * Upload for PostgreSQL 15.

 -- Christoph Berg <myon@debian.org>  Mon, 24 Oct 2022 16:40:35 +0200

postgresql-semver (0.31.2-3) unstable; urgency=medium

  * Adopt package in the PostgreSQL Team. (Closes: #1009988)

 -- Christoph Berg <myon@debian.org>  Mon, 16 May 2022 13:12:07 +0200

postgresql-semver (0.31.2-2) unstable; urgency=medium

  * Orphan.
  * Point Vcs fields to 'debian' namespace on Salsa.

 -- Felix Lechner <felix.lechner@lease-up.com>  Thu, 21 Apr 2022 09:45:04 -0700

postgresql-semver (0.31.2-1) unstable; urgency=medium

  [Christoph Berg]
  * Use `dh --with pgxs_loop` for simpler d/rules; also runs tests at build
    time (MR !1).

  [Felix Lechner]
  * New upstream version.
  * Update d/control with 'pg_buildext updatecontrol'. (Closes: #997721)
  * Drop two patches previously cherry-picked from upstream:
    - 0001-Use-INT32_MAX-instead-of-INT_MAX.patch
    - 0002-Handle-errors-from-strtol.patch
  * Place instructions for the upstream (orig) tarball in d/README.Debian.

 -- Felix Lechner <felix.lechner@lease-up.com>  Wed, 03 Nov 2021 09:20:04 -0700

postgresql-semver (0.31.1-3) unstable; urgency=medium

  * Hoping to fix autopkgtest on 32-bit architectures, cherry-pick
    commits 07b0c8b2 and 4d79dcc8 from upstream pursuant to
    https://github.com/theory/pg-semver/issues/58#issuecomment-927389762
  * Re-enable the TODO test disabled in commit 54d844e3.

 -- Felix Lechner <felix.lechner@lease-up.com>  Mon, 27 Sep 2021 16:10:33 -0700

postgresql-semver (0.31.1-2) unstable; urgency=medium

  * Disable TODO test that is expected to fail but succeeds on some
    architectures.
  * Move some Pg prerequisites from build to autopkgtest.
  * Bump Standards-Version to 4.6.0.

 -- Felix Lechner <felix.lechner@lease-up.com>  Tue, 14 Sep 2021 10:48:10 -0700

postgresql-semver (0.31.1-1) unstable; urgency=medium

  * Initial Debian release. (Closes: #986221)
  * Github tarball does not match repo; orig tarball was created by checking
    out tag v0.31.1 and running:
    - git ls-files | tar caf ../postgresql-semver_0.31.1.orig.tar.gz -T-

 -- Felix Lechner <felix.lechner@lease-up.com>  Fri, 07 May 2021 17:59:12 -0700
